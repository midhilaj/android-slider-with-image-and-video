package app.vyapari.vibanner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

import java.util.List;

public class VIBannerPagerAdapter extends PagerAdapter {

    List<VIBannerModel> mData;
    ImageView mImageView = null;
    VideoView mVideoView = null;
    private Context mContext;

    public VIBannerPagerAdapter(Context context, List<VIBannerModel> mData) {
        mContext = context;
        this.mData = mData;
    }

    public List<VIBannerModel> getmData() {
        return mData;
    }


    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {


        VIBannerModel modelObject = mData.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.vibanner, collection, false);
        mVideoView = layout.findViewById(R.id.iv_banner_videoView);
        mImageView = layout.findViewById(R.id.iv_baneer_imageView);


        if (mData.get(position).isIamImage()) {
            Glide.with(mContext).load(mData.get(position).getImageUrl()).into(mImageView);
            mVideoView.setVisibility(View.GONE);
            mImageView.setVisibility(View.VISIBLE);
        } else {
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mData.get(position).getVideoUrl(),
                    MediaStore.Images.Thumbnails.MINI_KIND);
            mImageView.setVisibility(View.VISIBLE);
            // Glide.with(mContext).load(mData.get(position).getVideoUrl()).into(mImageView);
            mVideoView.setVisibility(View.VISIBLE);

            BitmapDrawable bitmapDrawable = new BitmapDrawable(thumb);
            //mVideoView.setBackgroundDrawable(bitmapDrawable);
            //    Glide.with(mContext).asBitmap()
            //    .load(Uri.fromFile(new File(mData.get(position).getVideoUrl()))).into(mImageView);
        }


        collection.addView(layout);

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
        Log.i("destroyItem", position + "");
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
