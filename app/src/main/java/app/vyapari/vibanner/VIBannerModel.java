package app.vyapari.vibanner;

public class VIBannerModel {
    String videoUrl;
    String imageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9ZEbx7R8iNQKU-HlLqgM41sa5F-LeFNj4J9-c-404Lm6B20kP";
    boolean iamImage = true;
    boolean iamVideo = false;
    boolean videoAutoPay = false;

    public VIBannerModel() {

    }

    public VIBannerModel(String image) {
        setImageUrl(image);
        setIamImage(true);

    }

    public VIBannerModel(String fileUrl, boolean video) {
        if (video) {
            setVideoUrl(fileUrl);
            setIamVideo(true);
            setIamImage(false);
        } else {
            setIamImage(true);
            setIamVideo(false);
            setImageUrl(fileUrl);
        }

    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isIamImage() {
        return iamImage;
    }

    public void setIamImage(boolean iamImage) {
        this.iamImage = iamImage;
    }

    public boolean isIamVideo() {
        return iamVideo;
    }

    public void setIamVideo(boolean iamVideo) {
        this.iamVideo = iamVideo;
    }

    public boolean isVideoAutoPay() {
        return videoAutoPay;
    }

    public void setVideoAutoPay(boolean videoAutoPay) {
        this.videoAutoPay = videoAutoPay;
    }
}
