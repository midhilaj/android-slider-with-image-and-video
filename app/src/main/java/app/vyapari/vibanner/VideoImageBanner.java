package app.vyapari.vibanner;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;
import com.vincan.medialoader.MediaLoader;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class VideoImageBanner extends Fragment {
    VIBannerPagerAdapter mAdapter;
    SpringDotsIndicator mDotsIndicator;
    List<VIBannerModel> mData = new ArrayList<>();

    VideoView mVideoView;
    int mNowPlaying = 0;
    Runnable runnable;
    Handler mHandler;
    View view;
    ViewPager mViewPager;
    public VideoImageBanner() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.video_image_banner, container, false);
        mData.add(new VIBannerModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9ZEbx7R8iNQKU-HlLqgM41sa5F-LeFNj4J9-c-404Lm6B20kP"));
        mData.add(new VIBannerModel("https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4", true));
        mData.add(new VIBannerModel());
        mData.add(new VIBannerModel("https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_2mb.mp4", true));
        mData.add(new VIBannerModel());
        mAdapter = new VIBannerPagerAdapter(getContext(), mData);
        mViewPager = view.findViewById(R.id.vibannerview_vp);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mData.size(); i++) {
                    if (i == position) {
                        if (mData.get(i).isIamVideo()) {
                            if (mViewPager.getChildAt(i) != null) {
                                mVideoView = mViewPager.getChildAt(i).findViewById(R.id.iv_banner_videoView);

                                String proxyUrl = MediaLoader.getInstance(getContext()).getProxyUrl(mData.get(position).getVideoUrl());
                                mVideoView.setVideoPath(proxyUrl);
                                mVideoView.seekTo(1);
                                mVideoView.start();
                                mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mVideoView.start();
                                    }
                                });
                            }

                        }
                    } else {
                        if (mData.get(i).isIamVideo()) {
                            if (mViewPager.getChildAt(i) != null) {
                                mVideoView = mViewPager.getChildAt(i).findViewById(R.id.iv_banner_videoView);
                                mVideoView.stopPlayback();
                            }

                        }

                    }
                }

                mNowPlaying = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        runnable = new Runnable() {
            @Override
            public void run() {
                if (mData.get(mViewPager.getCurrentItem()).isIamVideo()) {
                    VideoView mVideoView = mViewPager.getChildAt(mViewPager.getCurrentItem()).findViewById(R.id.iv_banner_videoView);
                    if (mVideoView.isPlaying()) {
                        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                if (mData.size() == mViewPager.getCurrentItem()) {
                                    mNowPlaying = 0;
                                } else {
                                    mNowPlaying++;
                                }
                                if (mNowPlaying > mData.size()) {
                                    mNowPlaying = 0;
                                }
                                mViewPager.setCurrentItem(mNowPlaying);
                            }
                        });
                    } else {
                        if (mData.size() == mViewPager.getCurrentItem()) {
                            mNowPlaying = 0;
                        } else {
                            mNowPlaying++;
                        }
                        if (mNowPlaying > mData.size()) {
                            mNowPlaying = 0;
                        }
                        mViewPager.setCurrentItem(mNowPlaying);
                    }
                } else {
                    if (mData.size() == mViewPager.getCurrentItem()) {
                        mNowPlaying = 0;
                    } else {
                        mNowPlaying++;
                    }
                    if (mNowPlaying > mData.size()) {
                        mNowPlaying = 0;
                    }
                    mViewPager.setCurrentItem(mNowPlaying);
                }
                mHandler.postDelayed(runnable, 4500);
            }
        };

        mHandler = new Handler();
        mHandler.postDelayed(runnable, 4500);

        mDotsIndicator = (SpringDotsIndicator) view.findViewById(R.id.dots_indicator);

        mDotsIndicator.setViewPager(mViewPager);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler = null;
        mDotsIndicator = null;
        mData = null;
        mViewPager=null;

    }
}
